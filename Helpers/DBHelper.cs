﻿using NotesApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotesApplication.Helpers {
    class DBHelper {

        public void CreateDatabase(string path) {
            if (!CheckFileExists(path).Result) {
                using (SQLite.Net.SQLiteConnection connection = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path)) {
                    connection.CreateTable<Notes>();

                }
            }
        }
        private async Task<bool> CheckFileExists(string fileName) {
            try {
                var store = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                return true;
            } catch {
                return false;
            }
        }

        public ObservableCollection<Notes> ListNotes() {
            try {
                using (SQLite.Net.SQLiteConnection connection = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), App.DB_PATH)) {
                    List<Notes> myCollection = connection.Table<Notes>().ToList<Notes>();
                    ObservableCollection<Notes> NotesList = new ObservableCollection<Notes>(myCollection);
                    return NotesList;
                }
            } catch {
                return null;
            }
        }

        public Notes GetNote(int id) {
            using (SQLite.Net.SQLiteConnection connection = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), App.DB_PATH)) {
                var note = connection.Query<Notes>("select * from Notes where Id =" + id).FirstOrDefault();
                return note;
            }
        }

        public void AddNote(Notes note) {
            using (SQLite.Net.SQLiteConnection connection = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), App.DB_PATH)) {
                connection.RunInTransaction(() => {
                    connection.Insert(note);
                });
            }
        }

        public void EditNote(Notes note) {
            using (SQLite.Net.SQLiteConnection connection = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), App.DB_PATH)) {

                var existingconact = connection.Query<Notes>("select * from Notes where Id =" + note.Id).FirstOrDefault();
                if (existingconact != null) {

                    connection.RunInTransaction(() => {
                        connection.Update(note);
                    });
                }
            }
        }

        public void DeleteNote(int id) {
            using (SQLite.Net.SQLiteConnection connection = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), App.DB_PATH)) {

                var existingconact = connection.Query<Notes>("select * from Notes where Id =" + id).FirstOrDefault();
                if (existingconact != null) {
                    connection.RunInTransaction(() => {
                        connection.Delete(existingconact);
                    });
                }
            }
        }
    }
}
