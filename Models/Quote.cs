﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace NotesApplication.Models {

    [DataContract]
    class Quote {

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string title { get; set; }

        [DataMember]
        public string content { get; set; }

        [DataMember]
        public string link { get; set; }
    }
}
