﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotesApplication.Models {
    class Notes {
        
        [SQLite.Net.Attributes.PrimaryKey, SQLite.Net.Attributes.AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string Label { get; set; }
        public string CreatedAt { get; set; }
        public Notes() { }
        public Notes(string name, string content, string label) {
            Name = name;
            Content = content;
            Label = label;
            CreatedAt = DateTime.Now.ToString();
        }
    }
}
