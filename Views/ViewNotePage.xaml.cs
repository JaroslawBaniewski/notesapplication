﻿using NotesApplication.Helpers;
using NotesApplication.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace NotesApplication.Views {

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ViewNotePage : Page {

        Notes SelectedNote = new Notes();
        DBHelper dBHelper = new DBHelper();

        public ViewNotePage() {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) {

            SelectedNote     = e.Parameter as Notes;
            NameText.Text    = SelectedNote.Name;
            LabelText.Text   = SelectedNote.Label;
            ContentText.Text = SelectedNote.Content;
        }

        private void EditNote_Click(object sender, RoutedEventArgs e) {

            Frame.Navigate(typeof(EditNotePage), SelectedNote);
        }

        private void DeleteNote_Click(object sender, RoutedEventArgs e) {

            dBHelper.DeleteNote(SelectedNote.Id);
            MessageDialog messageDialog = new MessageDialog("Your note has been deleted");
            Frame.Navigate(typeof(MainPage));
        }

        private void MainPage_Click(object sender, RoutedEventArgs e) {

            Frame.Navigate(typeof(MainPage));
        }
    }
}
