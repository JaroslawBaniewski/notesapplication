﻿using NotesApplication.Helpers;
using NotesApplication.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace NotesApplication.Views {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EditNotePage : Page {

        Notes SelectedNote = new Notes();
        DBHelper dBHelper = new DBHelper();

        public EditNotePage() {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) {

            SelectedNote = e.Parameter as Notes;
            NameField.Text = SelectedNote.Name;
            LabelField.Text = SelectedNote.Label;
            ContentField.Text = SelectedNote.Content;
        }

        private async void EditNote_Click(object sender, RoutedEventArgs e) {

            // validate fields
            if (NameField.Text == "" || LabelField.Text == "" || ContentField.Text == "") {
                MessageDialog messageDialog = new MessageDialog("Some fields are not compeleted");
                await messageDialog.ShowAsync();
            }
            else {
                SelectedNote.Name = NameField.Text;
                SelectedNote.Label = LabelField.Text;
                SelectedNote.Content = ContentField.Text;
                dBHelper.EditNote(SelectedNote);
                MessageDialog messageDialog = new MessageDialog("Your note has been edited");
                Frame.Navigate(typeof(ViewNotePage), SelectedNote);
            }
        }

        private void MainPage_Click(object sender, RoutedEventArgs e) {

            Frame.Navigate(typeof(MainPage));
        }
    }
}
