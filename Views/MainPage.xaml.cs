﻿using Newtonsoft.Json;
using NotesApplication.Helpers;
using NotesApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Syndication;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace NotesApplication.Views {

    // for uote JSON parsing
    [DataContract]
    public class RootObject {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string content { get; set; }
        [DataMember]
        public string link { get; set; }
    }

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page {

        ObservableCollection<Notes> NotesList = new ObservableCollection<Notes>();

        public MainPage() {
            this.InitializeComponent();
            this.Loaded += LoadNotes;
        }

        private void LoadNotes(object sender, RoutedEventArgs e) {
            DBHelper dbHelper = new DBHelper();
            NotesList = dbHelper.ListNotes();
            listBox.ItemsSource = NotesList.OrderByDescending(i => i.Id).ToList();
        }

        private void AddNote_Click(object sender, RoutedEventArgs e) {
            Frame.Navigate(typeof(AddNotePage));
        }

        private void ListItem_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (listBox.SelectedIndex != -1) {
                Notes listItem = listBox.SelectedItem as Notes;
                Frame.Navigate(typeof(ViewNotePage), listItem);
            }
        }

        // send request to quotesondesign.com API
        private void GetQuote_Click(object sender, RoutedEventArgs e) {
            ShowQuote();
        }

        public async Task<RootObject> GetQuote() {

            var http = new HttpClient();
            var url = "https://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1&callback=";
            var response = await http.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();
            var serializer = new DataContractJsonSerializer(typeof(List<RootObject>));

            var ms = new MemoryStream(Encoding.UTF8.GetBytes(result));
            var QuoteData = (List<RootObject>)serializer.ReadObject(ms);
            ContentText.Text = QuoteData.First().content;
            return QuoteData.First();
        }

        private async void ShowQuote() {

            RootObject QuoteData = await GetQuote();
            if (QuoteData != null) {
                ContentText.Text = HtmlToPlainText(QuoteData.content);
                AuthorText.Text = WebUtility.HtmlDecode(QuoteData.title);
                LinkText.Text = QuoteData.link;
            }
        }

        public static string HtmlToPlainText(string source) {
            string content = Regex.Replace(source, "<.*?>", string.Empty);  // remove html tags
            return WebUtility.HtmlDecode(content);
        }
    }
}
